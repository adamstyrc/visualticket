package pl.systembiletowy.VisualTicket.service;

import pl.systembiletowy.VisualTicket.domain.*;
import pl.systembiletowy.VisualTicket.exception.InvalidTicketException;
import pl.systembiletowy.VisualTicket.exception.TicketServiceException;
import pl.systembiletowy.VisualTicket.utils.Deserializer;
import pl.systembiletowy.VisualTicket.utils.HttpUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

public class TicketService {

    private static TicketService INSTANCE;
    private static Properties properties = new Properties();

    static {

        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));

        try {
            properties.load(TicketService.class.getClassLoader().getResourceAsStream("visualticket.properties"));
        } catch (IOException e) {
            // suppress exception
        }
    }

    private final URL baseUrl;
    private final String charset;

    TicketService(URL baseUrl) {
        this(baseUrl, Charset.forName("UTF-8").name());
    }

    TicketService(URL baseUrl, String charset) {
        this.baseUrl = baseUrl;
        this.charset = charset;
    }

    public synchronized static TicketService getInstance() {
        return INSTANCE;
    }

    public synchronized static TicketService getInstance(String baseUrl) throws MalformedURLException {
        INSTANCE = new TicketService(new URL(baseUrl));
        return INSTANCE;
    }

    @SuppressWarnings("unused")
    public synchronized static TicketService getInstance(
            String baseUrl, String charset) throws MalformedURLException {
        INSTANCE = new TicketService(new URL(baseUrl), charset);
        return INSTANCE;
    }


    public LoginResponse login(String username, String password) throws IOException {
        String query = String.format("service.php/home/login.xml?login=%s&password=%s",
                    URLEncoder.encode(username, charset),
                    URLEncoder.encode(password, charset)
        );

        URL loginUrl = new URL(baseUrl, query);
        HttpURLConnection urlConnection = (HttpURLConnection) loginUrl.openConnection();
        try {
            configureConnection(urlConnection, "login");
            handleResponseCode(urlConnection);
            return Deserializer.parseAndClose(LoginResponse.class, urlConnection.getInputStream());
        } finally {
            urlConnection.disconnect();
        }
    }

    private void configureConnection(HttpURLConnection urlConnection, String prefix) {
        try {
            String readTimeout = properties.getProperty(prefix + ".read_timeout");
            if (readTimeout != null) {
                urlConnection.setReadTimeout(Integer.parseInt(readTimeout));
            }
        } catch (Exception e) {
            // suppress exception
        }

        try {
            String connectTimeout = properties.getProperty(prefix + ".connect_timeout");
            if (connectTimeout != null) {
                urlConnection.setReadTimeout(Integer.parseInt(connectTimeout));
            }
        } catch (Exception e) {
            // suppress exception
        }
    }

    private void handleResponseCode(HttpURLConnection urlConnection) throws IOException {
        if (urlConnection.getResponseCode() != 200) {
            InputStream is = urlConnection.getErrorStream();
            try {
                ErrorInfo errorInfo = Deserializer.parse(ErrorInfo.class, is);
                throw new TicketServiceException(errorInfo);
            } finally {
                is.close();
            }
        }
    }

    public LogoutResponse logout() throws IOException {
        URL logoutUrl = new URL(baseUrl, "service.php/home/logout.xml");
        HttpURLConnection urlConnection = (HttpURLConnection) logoutUrl.openConnection();
        try {
            configureConnection(urlConnection, "login");
            handleResponseCode(urlConnection);
            return Deserializer.parseAndClose(LogoutResponse.class, urlConnection.getInputStream());
        } finally {
            urlConnection.disconnect();
        }
    }

    public List<Event> getEvents() throws Exception {
        URL getEventsUrl = new URL(baseUrl, "service.php/repertoire/list.xml");
        HttpURLConnection urlConnection = (HttpURLConnection) getEventsUrl.openConnection();
        try {
            configureConnection(urlConnection, "events");
            handleResponseCode(urlConnection);
            return Deserializer.parseAndClose(EventList.class, urlConnection.getInputStream()).asList();
        } finally {
            urlConnection.disconnect();
        }
    }

    public Ticket validate(String eventId, String barcode) throws IOException {
        String query = String.format("service.php/usher/automatically.xml?repertoire_id=%s&code=%s",
                URLEncoder.encode(eventId, charset),
                URLEncoder.encode(barcode, charset));

        URL validateUrl = new URL(baseUrl, query);
        HttpURLConnection urlConnection = (HttpURLConnection) validateUrl.openConnection();
        try {
            configureConnection(urlConnection, "validate");
            handleResponseCode(urlConnection);
            String content = HttpUtils.readStreamAndClose(urlConnection.getInputStream());
            if (content.startsWith("<usher_error>")) {
                TicketError ticketError = Deserializer.parse(TicketError.class, content);
                throw new InvalidTicketException(ticketError);
            } else {
                return Deserializer.parse(Ticket.class, content);
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
