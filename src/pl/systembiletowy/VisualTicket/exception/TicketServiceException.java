package pl.systembiletowy.VisualTicket.exception;

import pl.systembiletowy.VisualTicket.domain.ErrorInfo;

public class TicketServiceException extends RuntimeException {

    private final ErrorInfo errorInfo;

    public TicketServiceException(ErrorInfo errorInfo) {
        this.errorInfo = errorInfo;
    }

    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }
}
