package pl.systembiletowy.VisualTicket.exception;

import pl.systembiletowy.VisualTicket.domain.TicketError;

public class InvalidTicketException extends RuntimeException {

    private final TicketError ticketError;

    public InvalidTicketException(TicketError ticketError) {
        this.ticketError = ticketError;
    }

    public TicketError getTicketError() {
        return ticketError;
    }
}
