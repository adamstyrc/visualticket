package pl.systembiletowy.VisualTicket.exception;

public class DeserializationException extends RuntimeException {

    public DeserializationException() {
    }

    public DeserializationException(String detailMessage) {
        super(detailMessage);
    }

    public DeserializationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DeserializationException(Throwable throwable) {
        super(throwable);
    }
}
