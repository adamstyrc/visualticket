package pl.systembiletowy.VisualTicket.utils;

import pl.systembiletowy.VisualTicket.domain.Event;

import java.util.*;

public class EventUtils {

    public static List<Event> todayEvents(List<Event> allEvents) {
        List<Event> todayEvents = new ArrayList<Event>();
        Calendar today = Calendar.getInstance();
        for (Event event : allEvents) {
            Calendar eventDay = Calendar.getInstance();
            eventDay.setTimeInMillis(event.getTimestamp() * 1000);
            if (today.get(Calendar.YEAR) == eventDay.get(Calendar.YEAR) &&
                today.get(Calendar.MONTH) == eventDay.get(Calendar.MONTH) &&
                today.get(Calendar.DAY_OF_MONTH) == eventDay.get(Calendar.DAY_OF_MONTH)) {
                todayEvents.add(event);
            }
        }
        return todayEvents;
    }
}
