package pl.systembiletowy.VisualTicket.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UserInfo {

    public static final String USER_PREFERENCES = "UserInfo";

    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String HOST = "host";
    private static final String SESSION = "session";
    private static final String EVENT_ID = "event_id";
    private static final String EVENT_TITLE = "event_title";
    private static final String EVENT_DATE = "event_date";
    private static final String EVENT_ATTENDEES = "event_attendees";
    private static final String ONLY_EAN13 = "only_EAN13";
    private static final String OFFLINE_MODE = "offline_mode";

    private SharedPreferences mUserPreferences;

    public UserInfo(Context context) {
        mUserPreferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
    }

    public String getLogin() {
        return mUserPreferences.getString(LOGIN, null);
    }

    public String getPassword() {
        return mUserPreferences.getString(PASSWORD, null);
    }

    public String getHost() {
        return mUserPreferences.getString(HOST, null);
    }

    public String getSession() {
        return mUserPreferences.getString(SESSION, null);
    }

    public String getEventId() {
        return mUserPreferences.getString(EVENT_ID, null);
    }

    public String getEventTitle() {
        return mUserPreferences.getString(EVENT_TITLE, null);
    }

    public String getEventDate() {
        return mUserPreferences.getString(EVENT_DATE, null);
    }

    public String getEventAttendees() {
        return mUserPreferences.getString(EVENT_ATTENDEES, null);
    }

    public void setLogin(String login) {
        putString(LOGIN, login);
    }

    public void setPassword(String password) {
        putString(PASSWORD, password);
    }

    public void setHost(String host) {
        putString(HOST, host);
    }

    public void setSession(String session) {
        putString(SESSION, session);
    }

    public void setEventId(String eventId) {
        putString(EVENT_ID, eventId);
    }

    public void setEventTitle(String eventTitle) {
        putString(EVENT_TITLE, eventTitle);
    }

    public void setEventDate(String eventDate) {
        putString(EVENT_DATE, eventDate);
    }

    public void setEventAttendees(String eventAttendees) {
        putString(EVENT_ATTENDEES, eventAttendees);
    }

    public void setOnlyEAN13(boolean onlyEAN13) {
        SharedPreferences.Editor editor = mUserPreferences.edit();
        editor.putBoolean(ONLY_EAN13, onlyEAN13);
        editor.commit();
    }

    public boolean isOnlyEAN13() {
        return mUserPreferences.getBoolean(ONLY_EAN13, true);
    }

    public void setMode(boolean mode) {
        SharedPreferences.Editor editor = mUserPreferences.edit();
        editor.putBoolean(OFFLINE_MODE, mode);
        editor.commit();
    }

    public boolean isOfflineMode() {
        return mUserPreferences.getBoolean(OFFLINE_MODE, false);
    }

    public boolean isLogged() {
        return getLogin() != null;
    }

    public void clear() {
        SharedPreferences.Editor editor = mUserPreferences.edit();
        editor.clear();
        editor.commit();
    }

    private void putString(String key, String value) {
        SharedPreferences.Editor editor = mUserPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
