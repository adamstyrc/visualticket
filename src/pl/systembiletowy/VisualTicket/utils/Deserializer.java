package pl.systembiletowy.VisualTicket.utils;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import pl.systembiletowy.VisualTicket.exception.DeserializationException;

import java.io.IOException;
import java.io.InputStream;

public class Deserializer {

    private static final Serializer SERIALIZER = new Persister();

    public static <T> T parse(Class<? extends T> type, InputStream is) {
        try {
            return SERIALIZER.read(type, is);
        } catch (Exception e) {
            throw new DeserializationException(e);
        }
    }

    public static <T> T parse(Class<? extends T> type, String content) {
        try {
            return SERIALIZER.read(type, content);
        } catch (Exception e) {
            throw new DeserializationException(e);
        }
    }

    public static <T> T parseAndClose(Class<? extends T> type, InputStream is) throws IOException {
        try {
            return SERIALIZER.read(type, is);
        } catch (Exception e) {
            throw new DeserializationException(e);
        } finally {
            is.close();
        }
    }
}
