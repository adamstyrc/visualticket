package pl.systembiletowy.VisualTicket.scanning;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import pl.systembiletowy.VisualTicket.App;
import pl.systembiletowy.VisualTicket.R;
import pl.systembiletowy.VisualTicket.domain.Ticket;
import pl.systembiletowy.VisualTicket.domain.TicketError;
import pl.systembiletowy.VisualTicket.exception.InvalidTicketException;
import pl.systembiletowy.VisualTicket.exception.TicketServiceException;
import pl.systembiletowy.VisualTicket.scanning.camera.CameraManager;
import pl.systembiletowy.VisualTicket.service.TicketService;
import pl.systembiletowy.VisualTicket.ui.EventListActivity;
import pl.systembiletowy.VisualTicket.ui.LoginActivity;
import pl.systembiletowy.VisualTicket.utils.UserInfo;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * This activity opens the camera and does the actual scanning on a background thread. It draws a
 * viewfinder to help the user place the barcode correctly, shows feedback as the image processing
 * is happening, and then overlays the results when a scan is successful.
 */
public final class CaptureActivity extends Activity implements SurfaceHolder.Callback {

    private static final String TAG = CaptureActivity.class.getSimpleName();

    private CameraManager cameraManager;
    private CaptureActivityHandler handler;
    private ViewfinderView viewfinderView;
    private TextView statusView;
    private View resultView;
    private ImageView flashlightImage;
    private boolean hasSurface;
    private InactivityTimer inactivityTimer;
    private BeepManager beepManager;
    private AmbientLightManager ambientLightManager;
    private Result savedResultToShow;
    private EditText manualInputEdit;
    private View manualInputSendButton;

    private boolean isManualInputMode = false;
    private AsyncTask<String,Void,Object> mValidateTicketTask;
    private View manualInputClearButton;
    private ProgressDialog pd;
    private ImageView barcodeImageView;

    ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    CameraManager getCameraManager() {
        return cameraManager;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.capture);

        setManualInputView();

        Button goBackButton = (Button) findViewById(R.id.go_back_button);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
        beepManager = new BeepManager(this);
        ambientLightManager = new AmbientLightManager(this);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }

    private void setManualInputView() {
        View manualInputView = View.inflate(this, R.layout.manual_input_layout, null);
        manualInputEdit = (EditText) manualInputView.findViewById(R.id.barcode_manual_edit);
        manualInputSendButton = manualInputView.findViewById(R.id.send_button);
        manualInputClearButton = manualInputView.findViewById(R.id.clear_button);
        manualInputSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String barcodeStr = manualInputEdit.getText().toString();
                if (!App.getUserInfo().isOnlyEAN13() || barcodeStr.length() == 13) {
                    Result result = new Result(barcodeStr, null, null, BarcodeFormat.EAN_13);
                    handleDecode(result, null, 0.5f);
                } else {
                    Toast.makeText(CaptureActivity.this, getString(R.string.invalid_ean13), Toast.LENGTH_SHORT).show();
                }
            }
        });

        manualInputClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualInputEdit.setText("");
            }
        });
        getActionBar().setCustomView(manualInputView);
    }

    @Override
    protected void onStart() {
        super.onStart();

        UserInfo userInfo = App.getUserInfo();
        if (!userInfo.isLogged()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else if (userInfo.getEventId() == null) {
            Intent intent = new Intent(this, EventListActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // CameraManager must be initialized here, not in onCreate(). This is necessary because we don't
        // want to open the camera driver and measure the screen size if we're going to show the help on
        // first launch. That led to bugs where the scanning rectangle was the wrong size and partially
        // off screen.
        cameraManager = new CameraManager(getApplication());

        viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        viewfinderView.setCameraManager(cameraManager);

        resultView = findViewById(R.id.result_view);
        barcodeImageView = (ImageView) findViewById(R.id.barcode_image_view);

        statusView = (TextView) findViewById(R.id.status_view);
        flashlightImage = (ImageView) findViewById(R.id.flashlight_view);
        flashlightImage.setTag(cameraManager.getTorchState());
        flashlightImage.setImageResource(cameraManager.getTorchState() ?
                R.drawable.flashlight_turn_off_icon :
                R.drawable.flashlight_turn_on_icon
        );
        flashlightImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flashlightImage.getTag().equals(Boolean.FALSE)) {
                    cameraManager.setTorch(true);
                    flashlightImage.setImageResource(R.drawable.flashlight_turn_off_icon);
                    flashlightImage.setTag(Boolean.TRUE);
                } else {
                    cameraManager.setTorch(false);
                    flashlightImage.setImageResource(R.drawable.flashlight_turn_on_icon);
                    flashlightImage.setTag(Boolean.FALSE);
                }
            }
        });

        handler = null;

        resetStatusView();

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (hasSurface) {
            // The activity was paused but not stopped, so the surface still exists. Therefore
            // surfaceCreated() won't be called, so init the camera here.
            initCamera(surfaceHolder);
        } else {
            // Install the callback and wait for surfaceCreated() to init the camera.
            surfaceHolder.addCallback(this);
        }

        beepManager.updatePrefs();
        ambientLightManager.start(cameraManager);

        inactivityTimer.onResume();
    }

    @Override
    protected void onPause() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        inactivityTimer.onPause();
        ambientLightManager.stop();
        cameraManager.closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_FOCUS:
            case KeyEvent.KEYCODE_CAMERA:
                // Handle these events so they don't launch the Camera app
                return true;
            // Use volume up/down to turn on light
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                cameraManager.setTorch(false);
                return true;
            case KeyEvent.KEYCODE_VOLUME_UP:
                cameraManager.setTorch(true);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (resultView.getVisibility() == View.VISIBLE) {
            restartPreviewAfterDelay(0L);
        } else {
            moveTaskToBack(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.capture, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem onlyEAN13Item = menu.findItem(R.id.menu_only_ean13);
        if (!App.getUserInfo().isOnlyEAN13()) {
            onlyEAN13Item.setTitle(getString(R.string.only_ean13));
        } else {
            onlyEAN13Item.setTitle(getString(R.string.all_codes));
        }

        MenuItem modeItem = menu.findItem(R.id.validation_mode);
        if (App.getUserInfo().isOfflineMode()) {
            modeItem.setTitle(R.string.online_mode);
        } else {
            modeItem.setTitle(R.string.offline_mode);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        switch (item.getItemId()) {
            case R.id.input_mode_toggle:
                isManualInputMode = !isManualInputMode;

                if (isManualInputMode) {
                    getActionBar().setDisplayShowCustomEnabled(true);
                    getActionBar().setDisplayShowTitleEnabled(false);
                    item.setIcon(R.drawable.ic_action_video);

                    manualInputEdit.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(manualInputEdit, InputMethodManager.SHOW_IMPLICIT);
                } else {
                    manualInputEdit.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(manualInputEdit.getWindowToken(), 0);

                    getActionBar().setDisplayShowCustomEnabled(false);
                    getActionBar().setDisplayShowTitleEnabled(true);
                    item.setIcon(R.drawable.ic_action_edit);

                }
//                mManualInputView.findViewById(R.id.)

                break;
            case R.id.menu_change_event:
                intent = new Intent(this, EventListActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_logout:
                App.logout(this);
                break;

            case R.id.menu_only_ean13:
                boolean onlyEAN13 = App.getUserInfo().isOnlyEAN13();
                App.getUserInfo().setOnlyEAN13(!onlyEAN13);
                break;

            case R.id.validation_mode:
                boolean offlineMode = App.getUserInfo().isOfflineMode();
                App.getUserInfo().setMode(!offlineMode);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // No op
    }

    private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
        // Bitmap isn't used yet -- will be used soon
        if (handler == null) {
            savedResultToShow = result;
        } else {
            if (result != null) {
                savedResultToShow = result;
            }
            if (savedResultToShow != null) {
                Message message = Message.obtain(handler, R.id.decode_succeeded, savedResultToShow);
                handler.sendMessage(message);
            }
            savedResultToShow = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (holder == null) {
            Log.e(TAG, "*** WARNING *** surfaceCreated() gave us a null surface!");
        }
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    /**
     * A valid barcode has been found, so give an indication of success and show the results.
     *
     * @param rawResult   The contents of the barcode.
     * @param scaleFactor amount by which thumbnail was scaled
     * @param barcode     A greyscale bitmap of the camera data which was decoded.
     */
    public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
        if (mValidateTicketTask != null && mValidateTicketTask.getStatus() != AsyncTask.Status.FINISHED) {
            return;
        }

        manualInputSendButton.setEnabled(false);
        inactivityTimer.onActivity();

        TextView formatText = (TextView) findViewById(R.id.format_text_view);
        formatText.setText(rawResult.getBarcodeFormat().name());

        TextView codeText = (TextView) findViewById(R.id.barcode_value);
        codeText.setText(rawResult.getText());

        String eventId = App.getUserInfo().getEventId();
        String barcodeStr = rawResult.getText();
        mValidateTicketTask = new ValidateBarcodeTask().execute(eventId, barcodeStr);
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder provided");
        }
        if (cameraManager.isOpen()) {
            Log.w(TAG, "initCamera() while already open -- late SurfaceView callback?");
            return;
        }
        try {
            cameraManager.openDriver(surfaceHolder);
            // Creating the handler starts the preview, which can also throw a RuntimeException.
            if (handler == null) {
                handler = new CaptureActivityHandler(this, null, null, null, cameraManager);
            }
            decodeOrStoreSavedBitmap(null, null);
        } catch (IOException ioe) {
            Log.w(TAG, ioe);
            displayFrameworkBugMessageAndExit();
        } catch (RuntimeException e) {
            // Barcode Scanner has seen crashes in the wild of this variety:
            // java.?lang.?RuntimeException: Fail to connect to camera service
            Log.w(TAG, "Unexpected error initializing camera", e);
            displayFrameworkBugMessageAndExit();
        }
    }

    private void displayFrameworkBugMessageAndExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.msg_camera_framework_bug));
        builder.setPositiveButton(R.string.button_ok, new FinishListener(this));
        builder.setOnCancelListener(new FinishListener(this));
        builder.show();
    }

    public void restartPreviewAfterDelay(long delayMS) {
        if (handler != null) {
            handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
        }
        resetStatusView();
    }

    private void resetStatusView() {
        String eventTitle = App.getUserInfo().getEventTitle();
        String eventDate = App.getUserInfo().getEventDate();
        String eventAttendees = App.getUserInfo().getEventAttendees();
        String text = String.format("Wydarzenie: %s\nData: %s\nLiczba uczestników: %s",
                eventTitle.substring(0, Math.min(40, eventTitle.length())),
                eventDate,
                eventAttendees
        );
        statusView.setText(text);
        statusView.setVisibility(View.VISIBLE);
        flashlightImage.setVisibility(View.VISIBLE);
        viewfinderView.setVisibility(View.VISIBLE);
        resultView.setVisibility(View.GONE);
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

    private class ValidateBarcodeTask extends AsyncTask<String, Void, Object> {


        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(CaptureActivity.this);
            pd.setTitle("Kasowanie biletu");
            pd.setMessage("Proszę czekać");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();

            statusView.setVisibility(View.GONE);
            flashlightImage.setVisibility(View.GONE);
            viewfinderView.setVisibility(View.GONE);
        }

        @Override
        protected Object doInBackground(String... params) {
            String eventId = params[0];
            String barcode = params[1];

            // handle offline mode - always validation success
            if (App.getUserInfo().isOfflineMode()) {
                Integer eventAttendees = Integer.parseInt(App.getUserInfo().getEventAttendees());
                return Ticket.valueOf(Integer.toString(eventAttendees+1), "", "", "", "");
            }

            // handle test event
            if ("1234".equals(eventId)) {
                try {
                    if (BigDecimal.ZERO.equals(new BigDecimal(barcode).remainder(BigDecimal.valueOf(2)))) {
                        return Ticket.valueOf("121", "B", "J", "49", "Krzysztof Styrc");
                    } else {
                        return TicketError.withErrorMessage("Bilet nieprawidłowy");
                    }
                } catch (Exception e) {
                    // ignore
                }
            }

            try {
                TicketService ticketService = TicketService.getInstance(App.getUserInfo().getHost());
                return ticketService.validate(eventId, barcode);
            } catch (TicketServiceException tse) {
                if (tse.getErrorInfo().getCode() == 903) {
                    // try to relogin
                    try {
                        TicketService ticketService = TicketService.getInstance();
                        ticketService.login(
                                App.getUserInfo().getLogin(),
                                App.getUserInfo().getPassword()
                        );
                        return ticketService.validate(eventId, barcode);
                    } catch (InvalidTicketException ite) {
                        return ite.getTicketError();
                    } catch (Exception e) {
                        return null;
                    }
                } else {
                    return null;
                }
            } catch (InvalidTicketException ite) {
                return ite.getTicketError();
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            if (pd!=null) {
                pd.dismiss();
            }

            if (o instanceof Ticket) {
                Ticket ticket = (Ticket) o;
                handleValidTicket(ticket);
            } else if (o instanceof TicketError) {
                TicketError ticketError = (TicketError) o;
                handleInvalidTicket(ticketError);
            } else {
                handleConnectionProblem();

            }

            manualInputSendButton.setEnabled(true);
        }
    }

    private void handleValidTicket(Ticket ticket) {
        clearResultView();
        beepManager.playOK();
        barcodeImageView.setImageResource(R.drawable.ok);

        TextView clientName = (TextView) findViewById(R.id.client_name);
        if (ticket.getCustomer() != null && ticket.getCustomer().getName() != null) {
            clientName.setText(ticket.getCustomer().getName());
        }

        TextView clientSection = (TextView) findViewById(R.id.client_section);
        if (ticket.getSection() != null) {
            clientSection.setText(ticket.getSection());
        }

        TextView clientRow = (TextView) findViewById(R.id.client_row);
        if (ticket.getRow() != null) {
            clientRow.setText(ticket.getRow());
        }

        TextView clientSeat = (TextView) findViewById(R.id.client_seat);
        if (ticket.getNumber() != null) {
            clientSeat.setText(ticket.getNumber());
        }

        TextView errorMessage = (TextView) findViewById(R.id.error_message);
        errorMessage.setText("");

        App.getUserInfo().setEventAttendees(ticket.getAttendees());

        resultView.setVisibility(View.VISIBLE);
    }

    private void handleInvalidTicket(TicketError ticketError) {
        clearResultView();
        beepManager.playNOK();
        beepManager.vibrate();

        barcodeImageView.setImageResource(R.drawable.error);
        TextView errorMessage = (TextView) findViewById(R.id.error_message);
        String message = ticketError.getMessage();
        errorMessage.setText(message);
        resultView.setVisibility(View.VISIBLE);
    }

    private void handleConnectionProblem() {
        Toast.makeText(this,
                "Problem z połączeniem",
                Toast.LENGTH_SHORT).show();
        restartPreviewAfterDelay(0L);
    }

    private void clearResultView() {
        TextView clientName = (TextView) findViewById(R.id.client_name);
        clientName.setText("");
        TextView clientSection = (TextView) findViewById(R.id.client_section);
        clientSection.setText("");
        TextView clientRow = (TextView) findViewById(R.id.client_row);
        clientRow.setText("");
        TextView clientSeat = (TextView) findViewById(R.id.client_seat);
        clientSeat.setText("");
        TextView errorMessage = (TextView) findViewById(R.id.error_message);
        errorMessage.setText("");
    }
}
