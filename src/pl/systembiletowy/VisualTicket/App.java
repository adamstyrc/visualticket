package pl.systembiletowy.VisualTicket;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import pl.systembiletowy.VisualTicket.service.TicketService;
import pl.systembiletowy.VisualTicket.ui.LauncherActivity;
import pl.systembiletowy.VisualTicket.utils.UserInfo;

public class App extends Application {

    private static Context sContext;

    private static UserInfo sUserInfo;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
        sUserInfo = new UserInfo(sContext);
    }

    public static UserInfo getUserInfo() {
        return sUserInfo;
    }

    public static void logout(Context context) {
        sUserInfo.clear();

        try {
            TicketService.getInstance().logout();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(context, LauncherActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

}
