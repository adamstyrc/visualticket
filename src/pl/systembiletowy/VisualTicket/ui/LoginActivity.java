package pl.systembiletowy.VisualTicket.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import pl.systembiletowy.VisualTicket.App;
import pl.systembiletowy.VisualTicket.R;
import pl.systembiletowy.VisualTicket.domain.ErrorInfo;
import pl.systembiletowy.VisualTicket.domain.LoginResponse;
import pl.systembiletowy.VisualTicket.exception.TicketServiceException;
import pl.systembiletowy.VisualTicket.service.TicketService;
import pl.systembiletowy.VisualTicket.utils.NetworkUtils;
import pl.systembiletowy.VisualTicket.utils.UserInfo;

public class LoginActivity extends Activity {

    UserInfo userInfo;

    private EditText usernameEdit;
    private EditText passwordEdit;
    private EditText baseUrlEdit;
    private Button mCheckButton;

    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_screen_layout);

        userInfo = App.getUserInfo();
        usernameEdit = (EditText) findViewById(R.id.username);
        passwordEdit = (EditText) findViewById(R.id.password);
        baseUrlEdit = (EditText) findViewById(R.id.baseUrl);

        if (savedInstanceState == null) {
            boolean isDebuggable =  ( 0 != ( getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE ) );
            if (isDebuggable) {
                usernameEdit.setText(getString(R.string.username_default));
                passwordEdit.setText(getString(R.string.password_default));

            }
        }


        mCheckButton = (Button) findViewById(R.id.check_button);
        mCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isOnline(LoginActivity.this)) {

                    new LoginTask().execute(
                            usernameEdit.getText().toString(),
                            passwordEdit.getText().toString(),
                            baseUrlEdit.getText().toString()
                    );
                } else {
                    Crouton.makeText(LoginActivity.this, R.string.no_network, Style.ALERT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Crouton.cancelAllCroutons();
    }

    private class LoginTask extends AsyncTask<String, Void, Object> {

        private String login;
        private String password;
        private String host;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(LoginActivity.this);
            pd.setTitle("Logowanie");
            pd.setMessage("Proszę czekać");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();
        }

        @Override
        protected Object doInBackground(String... params) {
            login = params[0];
            password = params[1];
            host = params[2];

            try {
                return TicketService
                        .getInstance(host)
                        .login(
                                login,
                                password
                        );
            } catch (Exception e) {
                if (e instanceof TicketServiceException) {
                    return ((TicketServiceException) e).getErrorInfo();
                } else {
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (pd!=null) {
                pd.dismiss();
            }

            if (result != null && result instanceof LoginResponse) {
                LoginResponse loginResponse = (LoginResponse) result;
                userInfo.setLogin(login);
                userInfo.setPassword(password);
                userInfo.setHost(host);
                userInfo.setSession(loginResponse.getSession());

                finish();

                Intent intent = new Intent(LoginActivity.this, EventListActivity.class);
                intent.putExtra(EventListActivity.MUST_CHOOSE, true);
                startActivity(intent);
            } else if (result != null && result instanceof ErrorInfo) {
                ErrorInfo errorInfo = (ErrorInfo) result;

                String text = getString(R.string.login_error) + " " + errorInfo.getMessage();
                Crouton.makeText(LoginActivity.this, text, Style.ALERT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
