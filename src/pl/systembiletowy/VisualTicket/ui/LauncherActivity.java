package pl.systembiletowy.VisualTicket.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import pl.systembiletowy.VisualTicket.App;
import pl.systembiletowy.VisualTicket.scanning.CaptureActivity;
import pl.systembiletowy.VisualTicket.utils.UserInfo;

public class LauncherActivity extends Activity {

    private UserInfo mUserInfo;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUserInfo = App.getUserInfo();

        if (!mUserInfo.isLogged()) {
            finish();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
//        } else if (mUserInfo.getEventId() == null) {
        } else {
            finish();
            Intent intent = new Intent(this, EventListActivity.class);
            intent.putExtra(EventListActivity.MUST_CHOOSE, true);
            startActivity(intent);
        }
//        } else {
//            finish();
//            Intent intent = new Intent(this, CaptureActivity.class);
//            startActivity(intent);
//        }
    }
}