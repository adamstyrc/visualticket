package pl.systembiletowy.VisualTicket.ui;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import pl.systembiletowy.VisualTicket.App;
import pl.systembiletowy.VisualTicket.R;
import pl.systembiletowy.VisualTicket.domain.Event;
import pl.systembiletowy.VisualTicket.exception.TicketServiceException;
import pl.systembiletowy.VisualTicket.scanning.CaptureActivity;
import pl.systembiletowy.VisualTicket.service.TicketService;
import pl.systembiletowy.VisualTicket.utils.EventUtils;
import pl.systembiletowy.VisualTicket.utils.UserInfo;

import java.util.ArrayList;
import java.util.List;

public class EventListActivity extends ListActivity {

    public static final String MUST_CHOOSE = "must choose, BACK blocked";

    private boolean mIsMustChoose;
    private ProgressBar progressBar;
    private ViewGroup root;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.events, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh_events:
                getListView().setAdapter(null);
                new GetEventListTask().execute();
                break;

            case R.id.menu_logout:
                App.logout(this);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        // Create a progress bar to display while the list loads
        progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(layoutParams);
        progressBar.setIndeterminate(true);
        getListView().setEmptyView(progressBar);

        // Must add the progress bar to the root of the layout
        root = (ViewGroup) findViewById(android.R.id.content);
        root.addView(progressBar);

        mIsMustChoose = getIntent().getBooleanExtra(MUST_CHOOSE, false);

        new GetEventListTask().execute();
    }

    private class GetEventListTask extends AsyncTask<Void, Void, List<Event>> {

        @Override
        protected List<Event> doInBackground(Void... params) {
            try {
                UserInfo userInfo = App.getUserInfo();
                TicketService.getInstance(userInfo.getHost())
                        .login(userInfo.getLogin(), userInfo.getPassword());
                List<Event> events = TicketService
                        .getInstance()
                        .getEvents();
                return EventUtils.todayEvents(events);
            } catch (TicketServiceException tse) {
                if (tse.getErrorInfo().getCode() == 903) {
                    // try to relogin and retry
                    try {
                        TicketService ticketService = TicketService.getInstance();
                        ticketService.login(
                                App.getUserInfo().getLogin(),
                                App.getUserInfo().getPassword()
                        );
                        return EventUtils.todayEvents(ticketService.getEvents());
                    } catch (Exception e) {
                        return null;
                    }
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Event> events) {
            if (events == null) {
                Toast.makeText(
                        EventListActivity.this,
                        R.string.get_events_failed,
                        Toast.LENGTH_SHORT
                ).show();
                events = new ArrayList<Event>();
            }

            if (events.isEmpty()) {
                Toast.makeText(
                        EventListActivity.this,
                        R.string.get_events_empty,
                        Toast.LENGTH_SHORT
                ).show();
            }

            events.add(Event.todayTestEvent());
            ArrayAdapter<Event> adapter = new ArrayAdapter<Event>(
                    EventListActivity.this,
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                    events
            );

            getListView().setEmptyView(progressBar);
            getListView().setAdapter(adapter);
            getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Event event = (Event) getListView().getItemAtPosition(position);

                    UserInfo userInfo = App.getUserInfo();
                    userInfo.setEventId(event.getId());
                    userInfo.setEventTitle(event.getTitle());
                    userInfo.setEventDate(event.getDate());
                    userInfo.setEventAttendees(event.getAttendees());

                    finish();
                    if (mIsMustChoose) {
                        Intent intent = new Intent(EventListActivity.this, CaptureActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }
}
