package pl.systembiletowy.VisualTicket.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import pl.systembiletowy.VisualTicket.App;
import pl.systembiletowy.VisualTicket.R;

public class SettingsActivity extends Activity {

    private View eventView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        setChangeEvent();
        setLogout();
    }

    @Override
    protected void onStart() {
        super.onStart();

        ((TextView) eventView.findViewById(R.id.description)).setText("Obecne wydarzenie: " + App.getUserInfo().getEventTitle());
    }

    private void setChangeEvent() {
        eventView = findViewById(R.id.change_event);
        ((TextView) eventView.findViewById(R.id.label)).setText("Zmień wydarzenie");
        ((TextView) eventView.findViewById(R.id.description)).setText("Obecne wydarzenie: " + App.getUserInfo().getEventTitle());
        eventView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, EventListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setLogout() {
        View logoutButton = findViewById(R.id.logout);
        ((TextView) logoutButton.findViewById(R.id.label)).setText("Wyloguj");
        ((TextView) logoutButton.findViewById(R.id.description)).setText("Kliknij, aby zmienić użytkownika");
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.logout(SettingsActivity.this);
            }
        });
    }


}