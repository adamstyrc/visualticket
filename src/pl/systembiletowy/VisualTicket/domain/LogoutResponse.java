package pl.systembiletowy.VisualTicket.domain;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "logout")
public class LogoutResponse {

    @Element(required = true)
    private String info;

    public String getInfo() {
        return info;
    }
}
