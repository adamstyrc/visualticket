package pl.systembiletowy.VisualTicket.domain;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "usher_error")
public class TicketError {

    @Element(name = "error_message", required = true)
    private String message;

    public String getMessage() {
        return message;
    }

    public static TicketError withErrorMessage(String errorMessage) {
        TicketError ticketError = new TicketError();
        ticketError.message = errorMessage;
        return ticketError;
    }
}
