package pl.systembiletowy.VisualTicket.domain;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import pl.systembiletowy.VisualTicket.scanning.CaptureActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Root(strict = false, name = "repertoire")
public class Event {

    @Attribute
    private String id;
    @Element(required = false)
    @Path("event[1]")
    private String title;
    @Attribute
    private String date;
    @Attribute
    private long timestamp;
    @Attribute
    private String type;
    @Attribute(name = "nb_of_entry")
    private String attendees;
    @Attribute(name = "free")
    private int freeSlots;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getType() {
        return type;
    }

    public String getAttendees() {
        return attendees;
    }

    public int getFreeSlots() {
        return freeSlots;
    }

    @Override
    public String toString() {
        return "" + title + " " + date;
    }

    public static Event valueOf(long timestamp) {
        Event event = new Event();
        event.timestamp = timestamp;
        return event;
    }

    public static Event todayTestEvent() {
        Event event = new Event();
        event.id = "1234";
        event.title = "Wydarzenie testowe";
        event.attendees = "0";

        long timestamp = Calendar.getInstance().getTimeInMillis();
        event.timestamp = timestamp / 1000;
        event.date = new SimpleDateFormat("dd MM yyyy HH:mm", Locale.US).format(timestamp);
        event.freeSlots = 5;
        event.type = "unknown";
        return event;
    }
}
