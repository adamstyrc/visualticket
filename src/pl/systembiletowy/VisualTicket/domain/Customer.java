package pl.systembiletowy.VisualTicket.domain;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class Customer {

    @Attribute
    private String barcode;

    @Element
    private String name;

    @Element(name = "image")
    private String imageUrl;

    public String getBarcode() {
        return barcode;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public static Customer valueOf(String name) {
        Customer customer = new Customer();
        customer.name = name;
        return customer;
    }
}
