package pl.systembiletowy.VisualTicket.domain;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "logged")
public class LoginResponse {

    @Attribute(name = "user_id", required = true)
    private int userId;

    @Element(required = false)
    private String name;

    @Element(required = false)
    private String email;

    @Element(required = false)
    private String login;

    @Element(required = false)
    private String session;

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getSession() {
        return session;
    }
}
