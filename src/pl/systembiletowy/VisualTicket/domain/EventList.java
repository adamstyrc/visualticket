package pl.systembiletowy.VisualTicket.domain;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(strict = false, name = "repertoires")
public class EventList {

    @ElementList(inline = true)
    private List<Event> events;

    public List<Event> asList() {
        return events;
    }
}
