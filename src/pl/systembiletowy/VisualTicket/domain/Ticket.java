package pl.systembiletowy.VisualTicket.domain;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "usher_ok")
public class Ticket {

    @Element(name = "nb_of_inputs")
    @Path("repertoire")
    private String attendees;

    @Element(required = false)
    @Path("repertoire")
    private String section;

    @Element(required = false)
    @Path("repertoire")
    private String row;

    @Element(required = false)
    @Path("repertoire")
    private String number;

    @Element(required = false)
    private Customer customer;

    public String getAttendees() {
        return attendees;
    }

    public String getSection() {
        return section;
    }

    public String getRow() {
        return row;
    }

    public String getNumber() {
        return number;
    }

    public Customer getCustomer() {
        return customer;
    }

    public static Ticket valueOf(String attendees, String section, String row, String number, String name) {
        Ticket ticket = new Ticket();
        ticket.attendees = attendees;
        ticket.section = section;
        ticket.row = row;
        ticket.number = number;
        ticket.customer = Customer.valueOf(name);
        return ticket;
    }
}
