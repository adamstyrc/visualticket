package pl.systembiletowy.VisualTicket.utils;

import org.junit.Test;
import pl.systembiletowy.VisualTicket.domain.Event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EventUtilsTest {

    @Test
    public void shouldReturnOnlyTodayEvents() throws Exception {
        // given
        List<Event> events = new ArrayList<Event>();
        Calendar calendar = Calendar.getInstance();
        // timestamp in seconds not milliseconds
        events.add(Event.valueOf(calendar.getTimeInMillis() / 1000));
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        events.add(Event.valueOf(calendar.getTimeInMillis() / 1000));
        calendar.add(Calendar.DAY_OF_MONTH, 2);
        events.add(Event.valueOf(calendar.getTimeInMillis() / 1000));

        // when
        List<Event> todayEvents = EventUtils.todayEvents(events);

        // then
        assertEquals(1, todayEvents.size());
    }
}
