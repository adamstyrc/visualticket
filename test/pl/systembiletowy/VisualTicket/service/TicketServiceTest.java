package pl.systembiletowy.VisualTicket.service;

import org.junit.Test;
import pl.systembiletowy.VisualTicket.domain.Event;
import pl.systembiletowy.VisualTicket.exception.InvalidTicketException;
import pl.systembiletowy.VisualTicket.exception.TicketServiceException;

import java.net.URL;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class TicketServiceTest {

    private static final String BASE_URL = "http://www.systembiletowy.pl/dev/";

    @Test(expected = TicketServiceException.class)
    public void shouldFailWithInvalidCredentials() throws Exception {
        // given
        TicketService ticketService = new TicketService(new URL(BASE_URL));

        // when & then
        ticketService.login("xxx", "yyy");
    }

    @Test
    public void shouldLoginToTheServer() throws Exception {
        // given
        TicketService ticketService = new TicketService(new URL(BASE_URL));

        // when & then
        ticketService.login("bileter", "1111");
    }

    @Test
    public void shouldLoginAndLogout() throws Exception {
        // given
        TicketService ticketService = new TicketService(new URL(BASE_URL));

        // when & then
        ticketService.login("bileter", "1111");
        ticketService.logout();
    }

    @Test
    public void shouldReturnListOfEvents() throws Exception {
        // given
        TicketService ticketService = new TicketService(new URL(BASE_URL));

        // when
        ticketService.login("bileter", "1111");
        List<Event> events = ticketService.getEvents();

        // then
        assertNotNull(events);
    }

    @Test(expected = TicketServiceException.class)
    public void shouldFailToGetEventsAfterLogout() throws Exception {
        // given
        TicketService ticketService = new TicketService(new URL(BASE_URL));

        // when & then
        ticketService.login("bileter", "1111");
        List<Event> events = ticketService.getEvents();

        assertNotNull(events);

        ticketService.logout();
        ticketService.getEvents();
    }

    @Test(expected = InvalidTicketException.class)
    public void shouldFailTicketValidation() throws Exception {
        // given
        TicketService ticketService = new TicketService(new URL(BASE_URL));

        // when & then
        ticketService.login("bileter", "1111");
        ticketService.validate("236", "1234");
    }

    @Test(expected = TicketServiceException.class)
    public void shouldFailTicketValidationAfterLogout() throws Exception {
        // given
        TicketService ticketService = new TicketService(new URL(BASE_URL));

        // when & then
        ticketService.login("bileter", "1111");
        ticketService.logout();
        ticketService.validate("1", "1");
    }
}
