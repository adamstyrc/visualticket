package pl.systembiletowy.VisualTicket.domain;

import org.junit.Test;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class UnmarshallingDomainObjectsTest {

    Serializer serializer = new Persister();

    @Test
    public void shouldDeserializeEvents() throws Exception {
        // given
        File source = getResourceFile("repertoires.xml");

        // when
        List<Event> events = serializer.read(EventList.class, source).asList();

        // then
        assertEquals(2, events.size());

        Event event = events.get(0);
        assertEquals("236", event.getId());
        assertEquals("Anna Karenina", event.getTitle()); // title present
        assertEquals("5 stycznia 2014 18:30", event.getDate());
        assertEquals("3", event.getType());

        assertEquals(1388943000, event.getTimestamp());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(event.getTimestamp() * 1000);
        assertEquals(2014, calendar.get(Calendar.YEAR));
        assertEquals(0, calendar.get(Calendar.MONTH));
        assertEquals(5, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(Calendar.PM, calendar.get(Calendar.AM_PM));
        assertEquals(6, calendar.get(Calendar.HOUR));
        assertEquals(30, calendar.get(Calendar.MINUTE));

        assertEquals("0", event.getAttendees());
        assertEquals(462, event.getFreeSlots());

        event = events.get(1);
        assertEquals("243", event.getId());
        assertNull(event.getTitle()); // title optional?
        assertEquals("5 stycznia 2014 18:30", event.getDate());
        assertEquals("3", event.getType());

        assertEquals(1388943000, event.getTimestamp());
        calendar.setTimeInMillis(event.getTimestamp() * 1000);
        assertEquals(2014, calendar.get(Calendar.YEAR));
        assertEquals(0, calendar.get(Calendar.MONTH));
        assertEquals(5, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(Calendar.PM, calendar.get(Calendar.AM_PM));
        assertEquals(6, calendar.get(Calendar.HOUR));
        assertEquals(30, calendar.get(Calendar.MINUTE));

        assertEquals("0", event.getAttendees());
        assertEquals(462, event.getFreeSlots());
    }

    @Test
    public void shouldDeserializeLogin() throws Exception {
        // given
        File source = getResourceFile("login.xml");

        // when
        LoginResponse loginResponse = serializer.read(LoginResponse.class, source);

        // then
        assertNotNull(loginResponse);
        assertEquals(4, loginResponse.getUserId());
        assertEquals("bileter", loginResponse.getLogin());
        assertEquals("Bileter", loginResponse.getName());
        assertEquals("info@systembiletowy.pl", loginResponse.getEmail());
        assertEquals("0j47qaau0amle567pllc9va2f3", loginResponse.getSession());
    }

    @Test
    public void shouldDeserializeLogout() throws Exception {
        // given
        File source = getResourceFile("logout.xml");

        // when
        LogoutResponse logoutResponse = serializer.read(LogoutResponse.class, source);

        // then
        assertNotNull(logoutResponse);
        assertEquals("Wylogowanie z systemu zakończone powodzeniem.", logoutResponse.getInfo());
    }

    @Test
    public void shouldDeserializeErrorInfo() throws Exception {
        // given
        File source = getResourceFile("error.xml");

        // when
        ErrorInfo errorInfo = serializer.read(ErrorInfo.class, source);

        // then
        assertNotNull(errorInfo);
        assertEquals(901, errorInfo.getCode());
        assertEquals("Brak użytkownika lub błędne hasło.", errorInfo.getMessage());
    }

    @Test
    public void shouldDeserializeTicketError() throws Exception {
        // given
        File source = getResourceFile("ticket_error.xml");

        // when
        TicketError ticketError = serializer.read(TicketError.class, source);

        // then
        assertNotNull(ticketError);
        assertEquals("Brak terminu lub nie posiadasz odpowiednich uprawnień. Skontaktuj się z administratorem.",
                ticketError.getMessage());
    }

    @Test
    public void shouldDeserializeTicketSuccessWithoutSeatInfoWithoutCustomerInfo() throws Exception {
        // given
        File source = getResourceFile("ticket_success_1.xml");

        // when
        Ticket ticket = serializer.read(Ticket.class, source);

        // then
        assertNotNull(ticket);
        assertEquals("2", ticket.getAttendees());
    }

    @Test
    public void shouldDeserializeTicketSuccessWithoutSeatInfo() throws Exception {
        // given
        File source = getResourceFile("ticket_success_2.xml");

        // when
        Ticket ticket = serializer.read(Ticket.class, source);

        // then
        assertNotNull(ticket);
        assertEquals("3", ticket.getAttendees());

        Customer customer = ticket.getCustomer();
        assertEquals("3000000184110", customer.getBarcode());
        assertEquals("Paweł Simka", customer.getName());
        assertEquals("/service.php/sbFilePlugin/download/file/92", customer.getImageUrl());
    }

    @Test
    public void shouldDeserializeTicketSuccess() throws Exception {
        // given
        File source = getResourceFile("ticket_success_3.xml");

        // when
        Ticket ticket = serializer.read(Ticket.class, source);

        // then
        assertNotNull(ticket);
        assertEquals("2", ticket.getAttendees());
        assertEquals("Parter", ticket.getSection());
        assertEquals("I", ticket.getRow());
        assertEquals("3", ticket.getNumber());

        Customer customer = ticket.getCustomer();
        assertEquals("3000000184110", customer.getBarcode());
        assertEquals("Paweł Simka", customer.getName());
        assertEquals("/service.php/sbFilePlugin/download/file/92", customer.getImageUrl());
    }

    private File getResourceFile(String filename) {
        URL url = this.getClass().getClassLoader().getResource(filename);
        return new File(url.getPath());
    }
}
