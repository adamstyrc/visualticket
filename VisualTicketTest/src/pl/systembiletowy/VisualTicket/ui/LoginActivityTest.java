package pl.systembiletowy.VisualTicket.ui;

import android.app.Activity;
import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import pl.systembiletowy.VisualTicket.R;
import pl.systembiletowy.VisualTicket.scanning.CaptureActivity;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onData;
import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.*;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.any;

public class LoginActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    public LoginActivityTest() {
        super(LoginActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testInvalidLogin() throws Exception {
        onView(withId(R.id.username))
                .perform(clearText(), typeText("xxx"));
        onView(withId(R.id.password))
                .perform(clearText(), typeText("1111"));
        onView(withId(R.id.check_button))
                .perform(click());

        // TODO how to check invalid login?
//        onView(withText(R.string.login_error))
//                .check(matches(withText(R.string.login_error)));
    }

    public void testValidLogin() throws Exception {
        Instrumentation.ActivityMonitor monitor =
                getInstrumentation().addMonitor(EventListActivity.class.getName(), null, false);

        onView(withId(R.id.username))
                .perform(clearText(), typeText("bileter"));
        onView(withId(R.id.password))
                .perform(clearText(), typeText("1111"));
        onView(withId(R.id.check_button))
                .perform(click());

        Activity activity = getInstrumentation().waitForMonitorWithTimeout(monitor, 5);
        assertNotNull(activity);
        assertTrue(activity instanceof EventListActivity);
        activity.finish();
    }

    public void testChooseEvent() throws Exception {
        Instrumentation.ActivityMonitor monitor =
                getInstrumentation().addMonitor(CaptureActivity.class.getName(), null, false);

        onView(withId(R.id.username))
                .perform(clearText(), typeText("bileter"));
        onView(withId(R.id.password))
                .perform(clearText(), typeText("1111"));
        onView(withId(R.id.check_button))
                .perform(click());

        onData(any(Object.class))
                .atPosition(1)
                .perform(click());

        Activity activity = getInstrumentation().waitForMonitorWithTimeout(monitor, 5);
        assertNotNull(activity);
        assertTrue(activity instanceof CaptureActivity);
        activity.finish();
    }
}
